import datetime

def xldate_to_date(xldate):
	temp = datetime.datetime(1899, 12, 30)
	delta = datetime.timedelta(days=xldate)
	return (temp+delta).strftime('%d/%m/%Y')

def ubah_bulan(bulan):
	dictb = {
		1 : "Januari",
		2 : "Februari",
		3 : "Maret",
		4 : "April",
		5 : "Mei",
		6 : "Juni",
		7 : "Juli",
		8 : "Agustus",
		9 : "September",
		10 : "Oktober",
		11 : "November",
		12 : "Desember",
	}
	return dictb[int(bulan)]