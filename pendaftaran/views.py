from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.template.loader import render_to_string

from django.conf import settings  # new
from django.core.files.storage import FileSystemStorage

from .models import *
from .forms import *

from .roman import int_to_Roman
from .datetime import *

import pytz
import datetime as dtime


import openpyxl
from openpyxl.styles import NamedStyle

# Create your views here.


def index(request):
    if request.method == 'GET':
        return render(request, 'pendaftaran/index.html')


def upload_file(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)

        if form.is_valid():
            # form.save()
            # return redirect('upload_file')

            # get data start here
            excel_file = request.FILES["document"]
            wb = openpyxl.load_workbook(excel_file)

            # worksheet = wb["Form Responses 1"]
            worksheet = wb.active
            # print(worksheet)

            excel_data = list()

            for row in worksheet.iter_rows(min_row=2, min_col=2, max_col=15, values_only=True):
                if row[0]!=None and row[1]!=None and row[2]!=None and row[3]!=None and row[6]!=None and row[9]!=None:
                    bio = Biodata()
                    bio.email = row[1].lower()
                    tt = row[0]
                    if isinstance(tt, dtime.datetime):
                        dt = tt.date()
                        tt = str(dt.day).zfill(2)+'/' + \
                            str(dt.month).zfill(2)+'/'+str(dt.year)
                        # excel_data.append(tt)
                        # excel_data.append(',, ')

                        bio.tanggal_pendaftaran = tt
                    else:
                        # dt = xldate_to_date(int(tt))
                        dt = str(tt).split(' ')[0].replace('-','/')
                        dtlist = dt.split('/')
                        bln = dtlist[0].zfill(2)
                        tgl = dtlist[1].zfill(2)
                        thn = dtlist[2].zfill(2)
                        dt = str(tgl)+'/'+str(bln)+'/'+str(thn)
                        bio.tanggal_pendaftaran = dt
                    bio.nama_lengkap = row[3].title()
                    bio.ttl = row[4].title().replace('/', ',')
                    bio.jenis_kelamin = row[5]
                    bio.asal_kampus = row[6]
                    bio.cabang_imiki = row[7]
                    bio.wilayah_imiki = row[8]
                    bio.angkatan = str(int(row[9]))
                    bio.no_hp = row[10]
                    bio.instagram = '@'+row[11].lower().replace('@', '')
                    bio.alamat = row[12]
                    bio.riwayat_penyakit = row[13].title(
                    ) if row[13] != '-' else 'Tidak Ada'

                    bio.save()
                    # excel_data.append(row[1])
                else:
                    pass
            # return HttpResponse(excel_data)

            return redirect('updatenomorpeserta')
    else:
        form = DocumentForm()
    return render(request, 'pendaftaran/uploadfile.html', {'form': form})


def updateNomorPeserta(request):
    bios = Biodata.objects.all()
    for v in bios:
        bio = Biodata.objects.get(pk=v.id)

        wilayah = int(v.wilayah_imiki.split()[1])
        dt = v.tanggal_pendaftaran
        kode_wilayah = int_to_Roman(wilayah)

        bio.nomor_peserta = 'MUNASX-'+kode_wilayah + \
            '-'+dt.split('/')[1]+'-'+str(v.id).zfill(4)
        bio.diedit = 'true'
        bio.save()

    return HttpResponse('sukses')


def form(request, id):
    if request.method == 'GET':
        b = Biodata.objects.get(pk=id)

        datetime_str = b.tanggal_pendaftaran.replace('/', '-')+' 00:00:00'
        date = dtime.datetime.strptime(datetime_str, '%d-%m-%Y %H:%M:%S').date()

        tanggal = date.day
        bulan = ubah_bulan(date.month)
        tahun = date.year

        tanggal_str = str(tanggal)+' '+str(bulan)+' '+str(tahun)

        data = {
            'nomor_peserta': b.nomor_peserta,
            'tanggal_pendaftaran': tanggal_str,
            'nama_lengkap': b.nama_lengkap,
            'ttl': b.ttl,
            'jenis_kelamin': b.jenis_kelamin,
            'asal_kampus': b.asal_kampus,
            'jurusan_prodi': 'Ilmu Komunikasi',
            'cabang_wilayah': b.cabang_imiki.title()+'/'+int_to_Roman(int(b.wilayah_imiki.split(' ')[1])),
            'angkatan': b.angkatan,
            'no_hp': b.no_hp,
            'email': b.email,
            'instagram': b.instagram,
            'alamat': b.alamat,
            'riwayat_penyakit': b.riwayat_penyakit
        }
        return render(request, 'formulir/isi.html', data)
