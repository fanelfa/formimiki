from django.db import models
from .validator import validate_file_extension

# Create your models here.
class Document(models.Model):
    document = models.FileField(upload_to='documents/', validators = [validate_file_extension])
    uploaded_at = models.DateTimeField(auto_now_add=True)

class Biodata(models.Model):
    nomor_peserta = models.CharField(max_length=255, blank=True)
    email = models.CharField(max_length=255, blank=True)
    tanggal_pendaftaran = models.CharField(max_length=255, blank=True)
    nama_lengkap = models.CharField(max_length=255, blank=True)
    ttl = models.CharField(max_length=255, blank=True)
    jenis_kelamin = models.CharField(max_length=255, blank=True)
    asal_kampus = models.CharField(max_length=255, blank=True)
    cabang_imiki = models.CharField(max_length=255, blank=True)
    wilayah_imiki = models.CharField(max_length=255, blank=True)
    angkatan = models.CharField(max_length=255, blank=True)
    no_hp = models.CharField(max_length=255, blank=True)
    instagram = models.CharField(max_length=255, blank=True)
    alamat = models.CharField(max_length=255, blank=True)
    riwayat_penyakit = models.CharField(max_length=255, blank=True)
    diedit = models.CharField(max_length=255, blank=True)
