from django.urls import path

from .views import *


urlpatterns = [
    path('', index, name='index'),
    path('upload', upload_file, name='upload_file'),
    path('update', updateNomorPeserta, name='updatenomorpeserta'),
    # path('simpandata/<int:id>', simpan_data, name='simpan_data'),
    path('form/<int:id>', form, name='form')
]
